/***************************************************************************
# WASH Monitor 1 (MONITOR) Firmware
# Copyright (c) 2018, Kjeld Jensen <kj@iug.dk> <kj@kjen.dk>
# Engineers Without Borders - Denmark http://iug.dk
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
# Revision
# 2018-09-03 KJ First released version
#****************************************************************************

# Board: "Arduino Pro or Pro Mini"
# Processor: "ATmega328P (3.3V, 8 MHz)"
# Please notice that at Gandarhun a prototype PCB based on Arduino Prob Mini
# (ATMega328, 16 MHz) is used.
# Please notice that at Nomo Faama the PCB is base on non-PU chips causing 
# bootloader installation errors.

# Remember to copy SBDLib to the Arduino libraries directory before compiling.
# Remember to remove connector from the Iridium modem before programming.

#***************************************************************************/

/* PCB type */
#define PCB_03-2018 /* PCB_03-2018 or outcomment for Arduino Pro mini prototype */

/* vfs flowsensor parameters */
#define VFS_5_100 /* VFS_2_40 or VFS_5_100 */
#define VFS_MEAS_INTERVAL 250 /* interval between measurements [ms] */

/* monitor parameters */
#define MONITOR_RST_TOUT 86396 /* [s] 24h-4s because watchdog sleeps 8s */
//#define MONITOR_RST_TOUT 1200 /* [s] TEST 20m */

/* iridium parameters */
#define IRIDIUM_TX_INTVAL 28680 /* [s] 8h-2m */
#define IRIDIUM_TX_TOUT 360 /* [s] */

/***************************************************************************/
/* includes */
#include <DallasTemperature.h>

extern "C"{
  #include <SBDLib.h>
};

/***************************************************************************/
/* defines and libraries */

/* monitor */
#define PIN_MON_RST_REQ A4 // (PC4/SDA) connected to WATCHDOG (PC4/SDA)

/* LED */
#define LED_HEARTBEAT 1 /* weak blink (1ms) */
#define LED_SIGNAL 100 /* powerful blink (100ms) */
#define LED_BLINK_INTERVAL 5000

/* DS18B20 temp sensor defines and libraries */
#define PIN_ONE_WIRE_BUS A3
OneWire oneWire(PIN_ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
#define TEMP_MEAS_INTERVAL 60000
#define TEMP_MIN_RESET 1000
#define TEMP_MAX_RESET -1000

/* iridium modem defines and libraries */
#define PIN_ROCKBLOCK_SLEEP 8
#define MODEM_STATE_IDLE 0
#define MODEM_STATE_INIT 1
#define MODEM_STATE_SBD_SEND_MSG 2

/* vfs flowmeter defines */
#define PIN_VFS_PWR 7 // (PD7)
#define VFS_FLOW_ADC_THRESHOLD_LOW 102  /* adc value corresponding to 0.5 Volt */
#define VFS_FLOW_ADC_THRESHOLD_HIGH 717  /* adc value corresponding to 3.5 Volt */

/* VFS 2-40 l/m */
/* calculation error is approx 0.05l/m at 40 l/m */
/* 2l/min = 102.4 [adc], 40l/min = 716.8 [adc]
   a = (40-2)*1e6/(716.8-102.4) = +61849
   b = 2000000 - 61849*102.4 = -4333338 */
#ifdef VFS_2_40
#define VFS_FLOW_A 61849
#define VFS_FLOW_B -4333338
#endif

/* VFS 5-100 l/m */
/* calculation error is approx 0.14l/m at 100 l/m */
/* 5l/min = 102.4 [adc], 100l/min = 716.8 [adc]
   a = (100-5)*1e6/(716.8-102.4) = +154622
   b = 5000000 - 154622*102.4 = -10833293 */
#ifdef VFS_5_100
#define VFS_FLOW_A 154622
#define VFS_FLOW_B -10833293
#endif

/* VFS temperature */
/* 0 deg = 102.4 [adc], 100 deg = 716.8 [adc]
   a = (100-0)*1e6/(716.8-102.4) = +162760
   b =  0 - 162760*102.4 = -16666667 */
#define VFS_TEMP_A 162920
#define VFS_TEMP_B -16666667

#define VFS_MEAS_PER_MIN (60000 / VFS_MEAS_INTERVAL) /* # measurements/min */

/***************************************************************************/
/* variables */
unsigned long time_ms; /* time since boot [ms] */
unsigned long time_s; /* time since boot [s] */
unsigned long led_next_blink; /* blink led next time [ms] */

/* iridium modem vars */
unsigned char msg_cnt;
char state;
char rssi;
char return_code;
char i;
char msg[50];
char msg_len;
unsigned char batt_v, batt_v_min; // battery voltage [V*10]
short temp, temp_min, temp_max; // temperature [degrees Celcius*10]
unsigned long next_meas;
unsigned long iridium_tx_started;
unsigned long iridium_next_tx;

/* vfs flowmeter vars */
unsigned short vfs_flow_adc; /* water flow [adc/min] */
unsigned long vfs_flow_ul_m; /* water flow [microliter/min] */
unsigned long vfs_vol_meas_ul; /* water volumen for this measurement [microliter] */
unsigned long vfs_vol_ul; /* water volumen [microliter] */
unsigned long vfs_vol_l; /* water volumen [liter] */
unsigned short vfs_temp_adc; /* water temperature [adc] */
unsigned long vfs_temp_deg; /* water temperature [degrees*10] */
unsigned short vfs_temp_min; /* lowest measured water temperature [degrees*10] */
unsigned long time_vfs_prev_meas;  /* time since boot to previous measurement [ms] */
unsigned long time_prev_status;  /* time since boot to previous status update [ms] */

/***************************************************************************/
void volt_init(void)
{
  batt_v_min = 255;
}
/***************************************************************************/
void volt_meas(void)
{
  // see ewb_mon1_batt_volt.ino for documentation
#ifdef PCB_03-2018
  batt_v = (((analogRead(A0) * 1500150L) >> 10) + 5000) / 10000;
#else
  batt_v = (((analogRead(A0) * 1719950L) >> 10) + 5000) / 10000;
#endif

  if (batt_v_min > batt_v)
    batt_v_min = batt_v;
}
/***************************************************************************/
void iridium_init()
{
  digitalWrite(PIN_ROCKBLOCK_SLEEP, LOW); // put the RockBlock in sleep mode (while still conf. as input)
  pinMode(PIN_ROCKBLOCK_SLEEP, OUTPUT); // configure the RockBlock pin as output
  state = MODEM_STATE_IDLE;
  Serial.begin(19200); // setup the serial port
  msg_cnt = 0;
  iridium_next_tx = millis() / 1000;
}
/***************************************************************************/
void iridium_send_msg()
{
  /* update the battery voltage */
  volt_meas();

  /* power up the iridium modem */
  digitalWrite(PIN_ROCKBLOCK_SLEEP, HIGH);
  delay(5000);

  /* activate iridium send */
  iridium_tx_started = time_s;
  sbd_init(time_s); // initialize Short Burst Data library
  state = MODEM_STATE_INIT;
}
/***************************************************************************/
void iridium_update()
{
  // if any serial data available
  while (Serial.available() > 0)
    sbd_rx_byte(Serial.read());

  // update the Short Burst Data library
  return_code = sbd_update(time_s);

  // state machine
  switch (state)
  {
    case MODEM_STATE_INIT:
      sprintf (msg, "M3,%d,%d,%d,%d,%ld,%d", msg_cnt, batt_v_min, temp_min, temp_max, vfs_vol_l, vfs_temp_min);
      msg_len = strlen(msg);
      sbd_auto_send_txt(msg, msg_len);
      state = MODEM_STATE_SBD_SEND_MSG;
      //blink_led(2, LED_SIGNAL);
      break;

    case MODEM_STATE_SBD_SEND_MSG:
      if (time_ms >= led_next_blink)
        blink_led (2, LED_HEARTBEAT);

      if (return_code != SBD_RETURN_WAIT)
      {
        if (return_code == SBD_RETURN_OK)
        {
          blink_led(3, LED_SIGNAL);
          state = MODEM_STATE_IDLE;
          msg_cnt++;
        }
        else
        {
          blink_led(4, LED_SIGNAL);
          if (time_s < iridium_tx_started + IRIDIUM_TX_TOUT)
          {
            delay (2000);
            state = MODEM_STATE_INIT;
          }
          else
          {
            state = MODEM_STATE_IDLE;
          }
        }
      }
      break;

    case MODEM_STATE_IDLE:
      digitalWrite(PIN_ROCKBLOCK_SLEEP, LOW); // Put the RockBlock to sleep
      break;
  }
}
/***************************************************************************/
void temp_reset()
{
  temp_min = TEMP_MIN_RESET;
  temp_max = TEMP_MAX_RESET;
}
/***************************************************************************/
void temp_init()
{
  sensors.begin(); // Start up the dallas sensor library
  next_meas = millis() / 1000;
  temp_reset();
}
/***************************************************************************/
void temp_meas()
{
  /* update the temperature */
  sensors.requestTemperatures(); // Send the command to get temperature readings
  temp = (sensors.getTempCByIndex(0) * 10);
  if (temp < temp_min)
    temp_min = temp;
  if (temp > temp_max)
    temp_max = temp;
}
/***************************************************************************/
void vfs_init()
{
  /* power on vfs flow sensor */
#ifdef PCB_03-2018
  pinMode(PIN_VFS_PWR, OUTPUT);
  digitalWrite(PIN_VFS_PWR, HIGH);
#endif

  vfs_vol_ul = 0;
  vfs_vol_l = 0;
  vfs_temp_min = 1023;
  time_vfs_prev_meas = millis() - VFS_MEAS_INTERVAL;
}
/***************************************************************************/
void vfs_meas()
{
  /* read vfs flow sensor */
  vfs_flow_adc = analogRead(A1);
  if (vfs_flow_adc < VFS_FLOW_ADC_THRESHOLD_LOW)
    vfs_flow_adc = 0;
  else if (vfs_flow_adc > VFS_FLOW_ADC_THRESHOLD_HIGH)
    vfs_flow_adc = VFS_FLOW_ADC_THRESHOLD_HIGH;

  if (vfs_flow_adc > 0)
  {
    vfs_flow_ul_m = vfs_flow_adc * VFS_FLOW_A + VFS_FLOW_B;
    vfs_vol_meas_ul =  vfs_flow_ul_m / 60 * (time_ms - time_vfs_prev_meas) / 1000;
    vfs_vol_ul += vfs_vol_meas_ul;
    while (vfs_vol_ul > 1000000)
    {
      vfs_vol_l += 1;
      vfs_vol_ul -= 1000000;
    }
  }
  else
  {
    vfs_flow_ul_m = 0;
  }

  /* read vfs temp sensor */
  vfs_temp_adc = analogRead(A2);
  vfs_temp_deg = vfs_temp_adc * VFS_TEMP_A + VFS_TEMP_B;
  vfs_temp_deg /= 100000;
  if (vfs_temp_deg < vfs_temp_min)
    vfs_temp_min = vfs_temp_deg;

  /* update measurement time variable */
  time_vfs_prev_meas = time_ms;
}
/***************************************************************************/
void vfs_update()
{
  /* update the time */
  if (time_ms >= time_vfs_prev_meas + VFS_MEAS_INTERVAL)
  {
    vfs_meas();
  }
}
/***************************************************************************/
void blink_led(char n, char ms)
{
  for (i = 0; i < n; i++)
  {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    delay (ms);
    digitalWrite(LED_BUILTIN, LOW);
    pinMode(LED_BUILTIN, INPUT);
    delay (300);
  }
  led_next_blink = millis() + LED_BLINK_INTERVAL;
}
/***************************************************************************/
void sbd_tx(char *s)
{
  Serial.print (s);
}
/***************************************************************************/
void setup()
{
  /* setup monitor */
  pinMode(PIN_MON_RST_REQ, OUTPUT);
  digitalWrite(PIN_MON_RST_REQ, HIGH);

  iridium_init(); /* initialize the iridium modem */
  volt_init(); /* initialize the voltage measurement */
  temp_init(); /* initialize the ds18b20 temperature sensor */
  vfs_init(); /* initialize vfs flowsensor */

  /* blink the LED a few times */
  blink_led(6, LED_SIGNAL);
}
/***************************************************************************/
void loop()
{
  time_ms = millis();
  time_s = time_ms / 1000;

  if (time_s >= MONITOR_RST_TOUT)
  {
    digitalWrite(PIN_MON_RST_REQ, LOW);
  }

  if (time_s >= next_meas)
  {
    temp_meas();
    volt_meas();
  }

  if (state == MODEM_STATE_IDLE && time_ms >= led_next_blink)
    blink_led (1, LED_HEARTBEAT);

  if (time_s >= iridium_next_tx)
  {
    iridium_send_msg();
    iridium_next_tx += IRIDIUM_TX_INTVAL;
  }

  vfs_update(); /* update VFS measurement */
  iridium_update(); /* update iridium modem */
}
/***************************************************************************/

