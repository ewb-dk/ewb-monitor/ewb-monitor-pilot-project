/***************************************************************************
# Iridium Short Burst Data library
# Copyright (c) 2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
Revision
2018-02-13 KJ First released test version
2018-08-31 KJ Added C declaration for Arduino
****************************************************************************/

#ifndef SBDLIB_H
#define SBDLIB_H


/***************************************************************************/
/* parameters */

/***************************************************************************/
/* defines */

/* sbd return codes */
#define SBD_RETURN_IDLE 0
#define SBD_RETURN_WAIT 1
#define SBD_RETURN_OK 2
#define SBD_RETURN_ERR 3
#define SBD_RETURN_ERR_LINK 4

/***************************************************************************/
/* global variables */

/***************************************************************************/
/* callback function prototypes */
extern void sbd_tx(char *s);

/***************************************************************************/
/* library function prototypes */
void sbd_init(unsigned long seconds);
char sbd_update(unsigned long seconds);
void sbd_modem_cfg(void);
void sbd_get_rssi(void);
char sbd_rssi(void);
void sbd_send_txt(char *m, unsigned short m_len);
void sbd_send_msg_bin(unsigned char *m, unsigned short m_len);
void sbd_auto_send_txt(char *m, unsigned short m_len);
void sbd_rx_byte(unsigned char b);
void sbd_rx(unsigned char *b, unsigned short b_len);

/***************************************************************************/
#endif 


