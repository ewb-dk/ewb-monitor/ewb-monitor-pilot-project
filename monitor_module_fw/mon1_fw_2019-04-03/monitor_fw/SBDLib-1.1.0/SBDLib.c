/***************************************************************************
# Iridium Short Burst Data library (SBDLib)
# Copyright (c) 2018-2019, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
Revision
2018-02-13 KJ (SBDLib 1.0.0) First released version 
2019-03-14 KJ (SBDLib 1.1.0) Simplified state machines. Added support for
              serial debug. Fixed bug when SBDIX returned 2 (location update
              not accepted).

****************************************************************************/
/* includes */
#include <string.h>
#include "SBDLib.h"

#ifdef DEBUG
	#include <stdio.h>
#endif

/***************************************************************************/
 
/* defines */
#define RXBUF_SIZE                  400
#define MSG_SIZE                    50

/* sbdlib auto states */
#define SBD_AUTO_IDLE                0

#define SBD_AUTO_SEND_TXT_CFG        1
#define SBD_AUTO_SEND_TXT_CFG_WAIT   2
#define SBD_AUTO_SEND_TXT_RSSI       3
#define SBD_AUTO_SEND_TXT_SEND       4

/* sbdlib states */
#define SBD_STATE_IDLE               0

#define SBD_STATE_CFG                1
#define SBD_STATE_CFG_WAIT           2

#define SBD_STATE_RSSI_REQ           3
#define SBD_STATE_RSSI_WAIT          4 

#define SBD_STATE_CNT_REQ            5
#define SBD_STATE_CNT_REQ_WAIT       6 

#define SBD_STATE_SEND_TXT_CPY       7
#define SBD_STATE_SEND_TXT_CPY_WAIT  8 
#define SBD_STATE_SEND_TXT_SYNC      9
#define SBD_STATE_SEND_TXT_SYNC_WAIT 10 

#define SBD_STATE_SEND_BIN_CPY       11
#define SBD_STATE_SEND_BIN_CPY_WAIT  12
#define SBD_STATE_SEND_BIN_SYNC      13 
#define SBD_STATE_SEND_BIN_SYNC_WAIT 14 

#define SBD_STATE_SYNC_BIN_TRY       15
#define SBD_STATE_SYNC_BIN_RDY_WAIT  16

#define SBD_STATE_MSG_REQ            17
#define SBD_STATE_MSG_REQ_WAIT       18


/***************************************************************************/
/* static variables */

static short i, j; /* must be signed because of for loop down to 0 */
static unsigned char rxbuf[RXBUF_SIZE];
static short rxbuf_len;
static unsigned char msg_buf[MSG_SIZE];
static unsigned short msg_len;
static char sbd_state, sbd_state_auto;
static unsigned char try;
static unsigned long tout;
static char ret_code, ret_code_auto;
static char mo_session_status;
static char rssi;
static unsigned long sys_time;

/***************************************************************************/
/* static function prototypes */
static short sbd_serial_rxbuf_get_first_line_end(void);
static void sbd_serial_rxbuf_remove_first_line(void);
static void sbd_serial_rx_reset(void);
#ifdef DEBUG
	static void sbd_debug_print_rxbuf (void);
#endif
static void sbd_state_auto_send_txt_update(void);

/***************************************************************************/
void sbd_init(void)
{
	sbd_state = SBD_STATE_IDLE;
	sbd_state_auto = SBD_AUTO_IDLE;
	#ifdef DEBUG
		printf ("SBD: Init\n");
	#endif
}
/***************************************************************************/
char sbd_cfg(unsigned long seconds)
{
	switch (sbd_state)
	{
		case SBD_STATE_IDLE:
			#ifdef DEBUG
				printf ("SBD: CFG Init\n");
			#endif
			try = 0;
			sbd_state = SBD_STATE_CFG;
			ret_code = SBD_WAIT;

		case SBD_STATE_CFG: /* Disable flow control and local echo */
			try++;
			sbd_serial_rx_reset();
			sbd_serial_tx("AT&K0E0\r");
			tout = seconds + TIMEOUT_AT; 
			sbd_state = SBD_STATE_CFG_WAIT;
			break;

		case SBD_STATE_CFG_WAIT: /* wait... */
			if (seconds >= tout) /* timeout check */
			{
				#ifdef DEBUG
					printf ("SBD: Timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_CFG;
				else
				{
					ret_code = SBD_ERR;
					sbd_state = SBD_STATE_IDLE;
				}
			}
			else /* incoming data check */
			{
				while (sbd_serial_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
						printf ("SBD: Received: %s\n", rxbuf);	
					#endif
					if (strstr((char *) rxbuf, "OK"))
          {
						ret_code = SBD_OK;
            sbd_state = SBD_STATE_IDLE;
          }
					sbd_serial_rxbuf_remove_first_line();
				}
			}
			break;
	}
	return ret_code;
}
/***************************************************************************/
char sbd_rssi (unsigned long seconds)
{
	switch (sbd_state)
	{
		case SBD_STATE_IDLE:
			#ifdef DEBUG
				printf ("SBD: RSSI Init\n");
			#endif
			try = 0;
			sbd_state = SBD_STATE_RSSI_REQ;
			ret_code = SBD_WAIT;

		case SBD_STATE_RSSI_REQ: /* request rssi */
			try++;
			sbd_serial_rx_reset();
			sbd_serial_tx("AT+CSQ\r");
			tout = seconds + TIMEOUT_CSQ; 
			sbd_state = SBD_STATE_RSSI_WAIT;
			break;

		case SBD_STATE_RSSI_WAIT: /* wait for response */
			if (seconds >= tout) /* timeout check */
			{
				#ifdef DEBUG
				printf ("SBD: Timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_RSSI_REQ;
				else
				{
					ret_code = SBD_ERR;
					sbd_state = SBD_STATE_IDLE;
				}
			}
			else 
			{
				while (sbd_serial_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
					printf ("SBD: Received line: %s\n", rxbuf);
					#endif
					if (rxbuf_len >= 6 && rxbuf[0] == '+' && rxbuf[1] == 'C'
					  && rxbuf[2] == 'S' && rxbuf[3] == 'Q' && rxbuf[4] == ':'
					  && rxbuf[5] >= '0' && rxbuf[5] <= '5')
					{
						rssi = rxbuf[5] - '0';
						ret_code = SBD_OK;
            sbd_state = SBD_STATE_IDLE;
					}
					sbd_serial_rxbuf_remove_first_line();
				}
			}
			break;
	}
	return ret_code;
}
/***************************************************************************/
char sbd_rssi_result(void)
{
	return rssi;
}
/***************************************************************************/
char hexval (char c)
{
	if (c >= '0' && c <= '9')
		c = c - '0';
	else if (c >= 'a' && c <= 'f')
		c = c - 'a' + 10;
	else if (c >= 'A' && c <= 'F')
		c = c - 'F' + 10;
	else
		c = -1;
	return c;
}
/***************************************************************************/
char sbd_check_msgs (unsigned long seconds)
{
	switch (sbd_state)
	{
		case SBD_STATE_IDLE:
			#ifdef DEBUG
				printf ("SBD: Check msg Init\n");
			#endif
			try = 0;
			sbd_state = SBD_STATE_MSG_REQ;
			ret_code = SBD_WAIT;
			break;

		case SBD_STATE_MSG_REQ: /* request messages available */
			try++;
			sbd_serial_rx_reset();
			sbd_serial_tx("AT+SBDIX\r");
			tout = seconds + TIMEOUT_SBDIX; 
			sbd_state = SBD_STATE_MSG_REQ_WAIT;
			break;

		case SBD_STATE_MSG_REQ_WAIT: /* wait for response */
			if (seconds >= tout) /* timeout check */
			{
				#ifdef DEBUG
				printf ("SBD: Timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_MSG_REQ;
				else
				{
					ret_code = SBD_ERR;
					sbd_state = SBD_STATE_IDLE;
				}
			}
			else 
			{
				while (sbd_serial_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
						printf ("SBD: Received: %s\n", rxbuf);
					#endif
					if (rxbuf[0] == '+' && rxbuf[5] == 'X') /* simple check for +SBDIX */
					{
						unsigned char mo;

						/* read MO status (dirty hack) */
						mo = rxbuf[8] - '0';
						if (rxbuf[9] != ',')
							mo = mo*10 + (rxbuf[9] - '0');
			
						if (mo == 0)
							ret_code = SBD_OK;
 						else
							ret_code = SBD_ERR;								
            sbd_state = SBD_STATE_IDLE;
					}
					sbd_serial_rxbuf_remove_first_line();
				}
			}
			break;
	}
	return ret_code;
}
/***************************************************************************/
short sbd_get_msg_result(void)
{
	short msg_num =1;
	return msg_num;
}
/***************************************************************************/
void sbd_send_msg (char *msg)
{
	msg_len = strlen(msg);
	memcpy (msg_buf, msg, msg_len+1); /* remember to include the trailing zero */
}
/***************************************************************************/
char sbd_send(unsigned long seconds)
{
	switch (sbd_state)
	{
		case SBD_STATE_IDLE:
			#ifdef DEBUG
				printf ("SBD: Send Init\n");
			#endif
			try = 0;
			sbd_state = SBD_STATE_SEND_TXT_CPY;
			ret_code = SBD_WAIT;	
			break;

		case SBD_STATE_SEND_TXT_CPY: /* send txt */
			try++;
			sbd_serial_rx_reset();
			sbd_serial_tx("AT+SBDWT=");
			sbd_serial_tx((char *) msg_buf);
			sbd_serial_tx("\r");

			tout = seconds + TIMEOUT_AT; 
			sbd_state = SBD_STATE_SEND_TXT_CPY_WAIT;

			#ifdef DEBUG
				printf ("SBD: Sending: AT+SBDWT=%s\n", (char *) msg_buf);
			#endif

			#ifdef DEBUG_SERIAL
				sprintf (debug_serial_s, "SBD: Send %s", (char *) msg_buf);
				sbd_debug_serial_tx (debug_serial_s);
			#endif
			break;

		case SBD_STATE_SEND_TXT_CPY_WAIT: /* wait for response */
			if (seconds >= tout) /* timeout check */
			{
				#ifdef DEBUG
					printf ("SBD: Timeout!\n");
				#endif

				#ifdef DEBUG_SERIAL
					sbd_debug_serial_tx ("SBD: Timeout");
				#endif

				if (try < TRY_MAX)
					sbd_state = SBD_STATE_SEND_TXT_CPY;
				else
				{
					ret_code = SBD_ERR;
					sbd_state = SBD_STATE_IDLE;
				}
			}
			else 
			{
				while (sbd_serial_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
						printf ("SBD: Received: %s\n", rxbuf);	
					#endif

					#ifdef DEBUG_SERIAL
						sprintf (debug_serial_s, "SBD in: %s", rxbuf);
						sbd_debug_serial_tx (debug_serial_s);
					#endif

					if (strstr((char *) rxbuf, "OK"))
					{
						sbd_state = SBD_STATE_SEND_TXT_SYNC;
						try = 0;
					}
					sbd_serial_rxbuf_remove_first_line();
				}
			}
			break;

		case SBD_STATE_SEND_TXT_SYNC: /* synchronize */
			try++;
			sbd_serial_rx_reset();
			sbd_serial_tx("AT+SBDIX\r");
			#ifdef DEBUG
			printf ("SBD: Sending: AT+SBDIX\n");
			#endif
			tout = seconds + TIMEOUT_SBDIX; 
			sbd_state = SBD_STATE_SEND_TXT_SYNC_WAIT;
			break;

		case SBD_STATE_SEND_TXT_SYNC_WAIT: /* wait for response */
			if (seconds >= tout) /* timeout check */
			{
				#ifdef DEBUG
				printf ("SBD: Timeout!\n");
				#endif
				if (try < TRY_MAX)
					sbd_state = SBD_STATE_SEND_TXT_SYNC;
				else
				{
					ret_code = SBD_ERR;
				}
			}
			else 
			{
				while (sbd_serial_rxbuf_get_first_line_end() != -1) /* incoming data check */
				{
					#ifdef DEBUG
					printf ("SBD: Received: %s\n", rxbuf);
					#endif
					if (rxbuf[0] == '+' && rxbuf[4] == 'I' && rxbuf[5] == 'X') /* simple check for +SBDIX */
					{
						/* read MO session status (with simple sanity check) */
						mo_session_status = -1;
						if (rxbuf[8] >= '0' && rxbuf[8] <= '9' && (rxbuf[9] == ',' || rxbuf[10] == ','))
						{
							mo_session_status = rxbuf[8] - '0';
							if (rxbuf[9] != ',' && rxbuf[9] >= '0' && rxbuf[9] <= '9')
							{
								mo_session_status = mo_session_status*10 + (rxbuf[9] - '0');
							}						
						}
						
						/* process MO session status */
						if (mo_session_status == 0) /* MO transferred succesfully */
							ret_code = SBD_OK;
						else if (mo_session_status == 2) /* MO transferred succesfully but loc req. not accepted */
							ret_code = SBD_OK;
 						else
							ret_code = SBD_ERR;								
            sbd_state = SBD_STATE_IDLE;
					}
					sbd_serial_rxbuf_remove_first_line();
				}
			}
			break;
	}
	return ret_code;
}
/***************************************************************************/
void sbd_send_bin_msg (unsigned char *msg, unsigned short len)
{
	msg_len = len;
	memcpy (msg_buf, msg, len);
}
/***************************************************************************/
char sbd_send_bin(unsigned long seconds)
{
	/* not yet implemented */
	return ret_code;
}
/***************************************************************************/
void sbd_serial_rx_byte(unsigned char b)
{
  if ((rxbuf_len + 1) < RXBUF_SIZE)
     rxbuf[rxbuf_len++] = b;
  else
    sbd_serial_rx_reset();
}
/***************************************************************************/
void sbd_serial_rx(unsigned char *b, unsigned short b_len)
{
	if ((b_len + rxbuf_len) < RXBUF_SIZE)
	{
		memcpy (rxbuf + rxbuf_len, b, b_len);
		rxbuf_len += b_len; 
	}
	else
		sbd_serial_rx_reset();
}
/***************************************************************************/
short sbd_serial_rxbuf_get_first_line_end(void)
{
	short line_end = -1;

	/* find first line end char */
	for (i=0; i<rxbuf_len; i++)
	{
		if ((rxbuf[i] == 13 || rxbuf[i] == 0) && line_end == -1)
		{
			line_end = i;
			rxbuf[i] = 0; /* convert to NULL to enable string functions */
		}
	}

	/*if (line_end != -1)
	{
		printf ("get line end %d\n", line_end);
		sbd_debug_print_rxbuf();
	}*/
	return line_end;
}
/***************************************************************************/
void sbd_serial_rxbuf_remove_first_line(void)
{
	short line_end = -1;

	/* find first line end char */
	for (i=0; i<rxbuf_len; i++)
	{
		if (line_end == -1 && (rxbuf[i] == 0 || rxbuf[i] == 13 || rxbuf[i] == 10))
			line_end = i;
	}
	i = line_end;

	if (i != -1)
	{
		/* include any neighbour line end chars */
		while (i<rxbuf_len && (rxbuf[i+1] == 13 || rxbuf[i+1] == 10 || rxbuf[i+1] == 0))
			i++;

			/* go to first char in the next line */
		if (i <rxbuf_len)
			i++;
 
		/* now delete from buffer */
		rxbuf_len -= i;
		for (j=0; j<rxbuf_len; i++, j++)
		{
			rxbuf[j] = rxbuf[i];
		}
		/* printf ("after removal: ");
		sbd_debug_print_rxbuf(); */

	}
}
/***************************************************************************/
static void sbd_serial_rx_reset(void)
{
	rxbuf_len = 0;
}
/***************************************************************************/
#ifdef DEBUG
void sbd_debug_print_rxbuf (void)
{
	if (rxbuf_len > 0)
	{
		printf ("SBD: RXBUF: ");
		for (i=0; i<rxbuf_len; i++)
		{
			if (rxbuf[i] == 0)
				printf ("[NULL]");
			else if (rxbuf[i] == 10)
				printf ("[LF]");
			else if (rxbuf[i] == 13)
				printf ("[CR]");
			else
				printf ("%c", rxbuf[i]);
		}
	printf ("[END] (length %d)\n", rxbuf_len);
	}
}
#endif
/***************************************************************************/
char sbd_auto_send(unsigned long seconds)
{
	switch (sbd_state_auto)
	{
		case SBD_AUTO_IDLE:
			#ifdef DEBUG
				printf ("SBD: Auto send Init\n");
			#endif
			sbd_state_auto = SBD_AUTO_SEND_TXT_CFG;
			ret_code_auto = SBD_WAIT;
			break;	
		
		case SBD_AUTO_SEND_TXT_CFG:
			sbd_cfg(seconds);
			sbd_state_auto = SBD_AUTO_SEND_TXT_CFG_WAIT;
			ret_code_auto = SBD_WAIT;

		case SBD_AUTO_SEND_TXT_CFG_WAIT:
			ret_code = sbd_cfg(seconds);
			if (ret_code != SBD_WAIT)
			{
				if (ret_code == SBD_OK)
				{
					sbd_state_auto = SBD_AUTO_SEND_TXT_RSSI;
				}
				else
				{
					sbd_state_auto = SBD_AUTO_IDLE;
					ret_code_auto = SBD_ERR;
				}

				#ifdef DEBUG
					if (ret_code == SBD_OK)
						printf ("SBD: Config OK\n");
					else if (ret_code == SBD_ERR)
						printf ("SBD: Config error\n");
					else
						printf ("SBD: Unknown return code: %d\n", ret_code);
				#endif
			}
			break;
			
		case SBD_AUTO_SEND_TXT_RSSI:
			ret_code = sbd_rssi(seconds);
			if (ret_code == SBD_OK)
			{
				rssi = sbd_rssi_result();
				sbd_state = SBD_STATE_IDLE;

				if (ret_code == SBD_OK) /* RSSI received ok and > 40% */
				{
					if (rssi >= 2)
					{
						sbd_state_auto = SBD_AUTO_SEND_TXT_SEND;
					}
					else
					{
						sbd_state_auto = SBD_AUTO_IDLE;
						ret_code_auto = SBD_ERR_LINK;					
					}
				}
				else
				{
					sbd_state_auto = SBD_AUTO_IDLE;
					ret_code_auto = SBD_ERR;
				}
				
				#ifdef DEBUG		
				if (rssi >= 0)
					printf ("SBD: RSSI: %d%%\n", rssi*20);
				else if (ret_code == SBD_ERR)
					printf ("SBD: RSSI ERROR\n");
				#endif

				#ifdef DEBUG_SERIAL
					sprintf (debug_serial_s, "SBD: RSSI %d", rssi*20);
					sbd_debug_serial_tx (debug_serial_s);
				#endif	
			}
			break;
			
		case SBD_AUTO_SEND_TXT_SEND:
			ret_code = sbd_send(seconds);
			if (ret_code != SBD_WAIT)
			{
				sbd_state = SBD_STATE_IDLE;
				if (ret_code == SBD_OK)
				{
					sbd_state_auto = SBD_AUTO_IDLE;
					ret_code_auto = SBD_OK;
				}
				else
				{
					sbd_state_auto = SBD_AUTO_IDLE;
					ret_code_auto = SBD_ERR;
				}

				#ifdef DEBUG
					if (ret_code == SBD_OK)
						printf ("SBD: Auto send text msg OK\n");
					else if (ret_code == SBD_ERR)
						printf ("SBD: Auto send text msg ERROR\n");
					else if (ret_code == SBD_ERR_LINK)
						printf ("SBD: Auto send text msg LINK ERROR\n");
					else 
						printf ("SBD: Unknown return code: %d\n", ret_code);
				#endif
			}
			break;
	}
	return ret_code_auto;
}
/***************************************************************************/

